<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/show13', ['uses'=>'Admin\IndexController@show13']);


Route::get('/', ['as'=>'home'/*,'middleware' => 'auth'*/,'uses'=>'Admin\IndexController@show']);

Route::get('/about', ['uses'=>'Admin\AboutController@show', 'as'=>'about']);
Route::get('/about/show_response', ['uses'=>'Admin\AboutController@showResponse', 'as'=>'show_response']);
//Route::get('/about/{id}', 'FirstController@show');

Route::get('/articles', ['uses'=>'Admin\Core@getArticles','as'=>'articles']);
Route::get('/articlesModel', ['uses'=>'Admin\Core@getArticlesModel','as'=>'articles']);

Route::get('contact', ['middleware' => ['auth'], 'uses'=>'Admin\ContactController@show', 'as' => 'contact']);
Route::post('contact', ['uses'=>'Admin\ContactController@store']);
//Route::match(['get', 'post'],'contact', ['uses'=>'Admin\ContactController@contact', 'as' => 'contact']);

//'middleware' => 'mymiddle' - регистрация класса посредника
//Route::get('/article/{page}', ['uses'=>'Admin\Core@getArticle', 'as'=>'article', 'middleware' => 'mymiddle']);//'middleware' => ['mymiddle', 'auth']
//Route::get('/article/{page}', ['middleware' => 'mymiddle:admin1','uses'=>'Admin\Core@getArticle', 'as'=>'article']);//->middleware(['mymiddle']);
Route::get('/article/{id}', ['uses'=>'Admin\Core@getArticle', 'as'=>'article']);//->middleware(['mymiddle']);


Route::group(['middleware' => ['web']], function(){

});

//Route::controller('/pages', 'PagesController'); - Устарел в 5.3 и был удалён

//----resource
//only - матоды для которых будут формироваться маршруты, если не указывать будет для всех
//except = !only
//Метод add ,будет добавлен к ресурсу ниже
//Route::get('pages/add','Admin\CoreResource@add');
//Route::resource('/pages','Admin\CoreResource',['except'=>['index','show']]);
//----resource
//Route::get('article/{id}', ['as'=>'article',function ($id) {
//    echo $id;
//}]);
//
Route::get('/form', function () {
    return view('form');
});
//
////Переменные передаються по порядку, не по имени
////cat? - данный параметр не обязателен
////Route::get('page/{id}/{cat?}', function($var1, $var2 = 50){
//Route::get('page/{id}/{cat}', function($var1, $var2){
//
//    echo '<pre>';
////        print_r($_ENV);//Настройки
////    echo config('app.locale');
////    echo Config::set('app.locale', 'ru');
////    echo Config::get('app.locale');
////    echo env('APP_ENV');
//
//        echo $var1;
//        echo $var2;
//
//    echo '</pre>';
//
////    return view('page');
//});//->where(['id' => '[0-9]+', 'cat' => '[A-Za-z]+']);
////})->where('id', '[0-9]+');
//
////Route::post('comments', function(){
////    print_r($_POST);
////});
//
////Route::match(['get','post'],'comments', function(){
////    print_r($_POST);
////});
//
////Для всех типов запроса
////Route::any('/comments', function(){
////    print_r($_POST);
////});
//
//                        //Группа маршрутов
////admin/page/create     - ['prefix' => 'admin']
////admin/id/page/create  - ['prefix' => 'admin/{id}']
//Route::group(['prefix' => 'admin'], function(){
//
//    Route::get('page/create', function(){
//        return redirect()->route('article', ['id'=>25]);
//    });
//
//    Route::get('page/edit/{var}', function($id){
//
//        $route = Route::current();
//        echo $route->getName()."<br>";
//        print_r($route->parameters());
//        echo "<br>";
//
////        echo 'page/edit';
//    })->name('page_edit');
//
//});

Auth::routes();
//admin/{}/
Route::group(['prefix' => 'admin', 'middleware' => ['web', 'auth']], function(){
    Route::get('/',['uses' => 'Admin\AdminController@show', 'as' => 'admin_index']);
    Route::get('/add/post',['uses' => 'Admin\AdminPostController@create', 'as' => 'admin_add_post']);
});

////////////
//Auth::routes();
//
Route::get('/home', 'HomeController@index')->name('home');
