<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Создание собственной директивы
//        Blade::directive('myDir', function($var){
//            return "<h1>New Directive - $var</h1>";
//        });


        //Отслеживать все SQL запросы и параметры к ним
        DB::listen(function($query){
//            dump($query->sql);
//            dump($query->bindings);
        });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
