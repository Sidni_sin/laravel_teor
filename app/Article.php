<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes; // Трейт для мягкого удаления

    protected $datas = ['deleted_at'];

//    protected $table = 'articles';

//    protected $primaryKey = 'id'; // По умолчанию
//    public $incrementing = true; // По умолчанию

//    public $timestamps = false;//Чтобы отключить дату

    protected $fillable = ['name', 'text', 'img', 'alias'];//Массив в которые можно записывать с помощью create
//    protected $guarded = ['*'];//зАПРЕЩЁННЫЕ ДЛЯ ЗАПИСИ

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
