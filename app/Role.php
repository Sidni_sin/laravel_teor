<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name',
    ];

    public function users()
    {
        //2 - role_user default
        //3 - role_id
        //4 - user_id
        return $this->belongsToMany('App\User');
    }
}
