<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{


    public function user()
    {
        //Структура такая же как и у hasOne
        return $this->belongsTo('App\User');
    }

}
