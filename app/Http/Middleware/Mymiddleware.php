<?php

namespace App\Http\Middleware;

use Closure;

class Mymiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $param)//Параметры 3 и больше, передаються те параметры которые указываються при регистрации посредника
    {
        //$next - передаёт дальше по цепочке в middleware или в приложение
        //route()
        //route('name_param')
        if($request->route('page') != 'pages' && $param == 'admin'){
            return redirect()->route('home');
        }

        return $next($request);
    }

}
