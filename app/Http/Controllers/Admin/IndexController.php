<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function show()
    {

//        view('welcome');
//        return view('default.template', $data);

        //Проверка на существование шаблона
        if(view()->exists('default.about')){

            $data = ['title'=>'Title', 'title2' => 'Title2'];

            return view('default.about', $data);

//            $view_in_string = view('default.template', $data)->render();
//            $path_in_string = view('default.template', $data)->getPath();
//            echo $path_in_string;
//
//            return $view_in_string;
//            $path = config('view.paths');
//            return view()->file($path[0].'/default/template.php')->with($data);
//            return view('default.template')->with($data);
        }

        //Ответ 404
        abort(404);

    }



    public function show13()
    {
        $arr = [
            'title'=>'Lara Proj',
            'data'=>[
                'one' => 'List 1',
                'two' => 'List 2',
                'there' => 'List 3',
                'four' => 'List 4',
                'five' => 'List 5',
            ],
            'dataI' => ['List 1','List 2','List 3','List 4','List 5',],
            'bvar'=>true,
            'script'=>'<script>alert("hello")</script>',
        ];

        if(view()->exists('default.index')){
            return view('default.index', $arr);
        }

        abort(404);

    }

}
