<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ContactRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator;

class ContactController extends Controller
{

//    protected $request;
//
//    public function __construct(Request $request)
//    {
//        $this->request = $request;
//    }
    public function store(Request $request, $id=NULL)
    {

        if($request->isMethod('post')){

            $message = [
                'name.required' => 'Нужно заполнить поле :attribute',
            ];

            $validator = Validator::make($request->all(), [
                'name' => 'required'
            ], $message);

            if($validator->fails()){
                return redirect()->route('contact')->withErrors($validator)->withInput();
            }





            /////////////////////
            /*
             * lesson 25
             *
            $rules = [
                'name'  => 'after:tomorrow', //Болие поздняя дата
//                'name'  => 'required|max:10',
//                'email' => 'required|email',
            ];
            //Если валидация не пройдено будет исключения и редирект обратно на стр
            //в сессию error будут ошибки
            $this->validate($request, $rules);

            //kod
            dump($request->all());
*/


        }
        return view('default.contact', ['title' => 'Contact']);
    }

    public function show()
    {
        return view('default.contact', ['title' => 'Contact']);
    }



    public function showView(Request $request, $id=NULL)
    {

        echo '<br><br>';
        echo '<br><br>';

//        dump($request->all());
//        dump($request->only('name', 'site'));//Возвратит name, site
//        dump($request->except(['name', 'site']));//Возвратит всё кроме name, site

//            echo $request->id;
//            //Сначало к данным формы потом к параметру запроса
//            echo $request->name;

//        dump($request->has('name'));//Странно он возвращяет true если ессть ключ массива

//            echo "<h1>$id<h1>";
//            echo "<h1>".$request->input('name')."<h1>";
//            echo "<h1>".$request->input('name11111','Default')."<h1>";

        //return URI
//        echo $request->path();
//
//        //Соответсвует ли запрос параметру
//        if($request->is('contact/*')){
//            echo $request->path();
//        }
        //return URL - запрос без гет параметров
//        echo $request->url();
        //return URL - запрос c гет параметров
//        echo $request->fullUrl();

        //Тип запроса
//        echo $request->method();

        //Домен проекта
//        echo $request->root();
//        //Взять параметр из get запроса
//        echo $request->query('option');
//
//        dump($request->header('host'));
//        dump($request->header());

//        dump($request->server());// == $_SERVER
//        dump($request->segments());

//        $request->flush();//Очистить сессии
//        $request->old('name');

            if($request->isMethod('GET')){
                echo $request->method();
            }elseif($request->isMethod('POST')){
//
//
//                //Добавляет все данные из $request
//                $request->flash();

//
//                $request->flashOnly('name', 'site');
//                $request->flashExcept('name', 'site');
//
//                echo $request->method();

//              Сохраняем данные объекта request в сессию
//                return redirect()->route('contact')->withInput();//Чёт не работает
//
            }



//        print_r($request->all());

        return view('default.contact', ['title' => 'Contact']);
    }


}
