<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Country;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;


class Core extends Controller
{

    protected static $article;

    public function __construct()
    {
//        $this->middleware('mymiddle');
    }

    public static function addArticles($array)
    {
        return self::$article[] = $array;
    }   

    public function getArticles()
    {
        //У модели пользователя юзер есть связанные данные в таблице артиклес
        //2 и болие связанных записей
//        $u = User::has('articles', '>=', '2')->get();
//        $u = User::with('articles','roles')->get();

//        $a = Article::all();
        //Манипулация с моделью ...

        //Дозагрузка связанных данных
//        $a->load('user');


        //Загрузки данных связанных моделей
//        $a = Article::with('user')->get();



//        foreach ($u as $i){
////            dump($i->roles);
//            dump($i->articles);
//        }

        //Добавление связанных данных
        $user = User::find(1);
//        $article = new Article([
//            'name' => 'New Art',
//            'text' => 'Some text'
//        ]);
        //save - использует модель
        //create - использует массив
//        $user->articles()->save($article);
//        $user->articles()->save([
//            'name' => 'New Art',
//            'text' => 'Some text'
//        ]);
        //Сохранить несколько моделей сразу
//        $user->articles()->saveMany([
//            new Article(['name' => 'New Art', 'text' => 'Some text']),
//            new Article(['name' => 'New Art1', 'text' => 'Some text1'])
//        ]);

//        $role = new Role(['name' => 'quest']);
//        $user->roles()->save($role);

        $user->articles()->where('id', 20)->update(['name' => 'update']);

        $a = Article::find(20);

        dump($a);



//        dump($a);

        return;
        //Многие ко многим

//        $role = Role::find(1);
//        dump($role->users);

        ///

//        $user = User::find(1);

//        dump($user->roles);
//        dump($user->roles()->where('roles.id',1)->first());


        //Один ко многим

//        $user = User::find(1);
//        $articles = $user->articles;
        //Если обращяться к мутоду а не к свойству то можно формировать запрос
//        $articles = $user->articles()->where('id', '2')->first();
//        dump($articles);

//        $article = Article::find(2);
//
//        dump($article->user->name);


//        foreach ($articles as $a){
//            echo $a->name."<br>";
//        }

        //Один к одному
        ///////////////////////
//        $country = Country::find(1);
//        $user = User::find(1);

//        $user->country;// Связь

//        $country = $user->country;
//        dump($country->user);

    }

    public function getArticlesModel()
    {

//        Article::create(['name' => 'name t', 'text' => 'test t', 'img' => null, 'alias' => null]);

        //возвращает найденную запись если она есть, если нет сохранит данные
//        $a = Article::firstOrCreate(['name' => '22name t', 'text' => 'test t', 'img' => null, 'alias' => null]);//Добавление с проверкой на уникальность по первому елементу


        //Тоже самое но возвращает объект модель
//        Article::firstOrNew(['name' => '22name t', 'text' => 'test t', 'img' => null, 'alias' => null]);
//        Article::save();

//        $m = Article::find(6);
//        $m->delete();

//        Article::destroy([4,2]);

//Мягкое удаление
        $a = Article::find(2);
        $a != null ? $a->delete() : null;

        $a = $a->forceDelete();//Полностью удалить запись

//        $a_l_d = Article::withTrashed()->get(); //Вытащий записи в месте с мягким удалением
//        $a_l_d = Article::withTrashed()->restore(); //Выбрать всё и востановить удалённые записи
//        $a_l_d = Article::onlyTrashed()->restore(); //Выбрать только удалённые и востановить их

        /*
        foreach ($a_l_d as $item){
            //Если запись была удалена в корзину
            if($item->trashed()){
                echo $item->id.' Delete<br>';
                $item->restore(); //Убрать из корзины удаления
            }else{
                echo $item->id.' No Delete<br>';
            }
        }
*/
//        dump($a_l_d);


        $a = Article::all();
        dump($a);
    }

    //list materials
    public function getArticlesLerning()
    {

//        DB::table('articles')->get();
//        DB::table('articles')->first();
//        DB::table('articles')->value('name'); // первыя запись конкретного поля
//        DB::table('articles')->chunk(2, function($articles){ //выберает по N записей и отправляет их в колбек функцию
//            foreach($articles as $article){
//                Core::addArticles($article);
//            }
//        });

//        $articles = DB::table('articles')->pluck('name');//все записи по полю
//        $articles = DB::table('articles')->count();
//        $articles = DB::table('articles')->max('id');
//        $articles = DB::table('articles')->distinct()->select('name')->get();
//        $query = DB::table('articles')->select('name');
//        $articles = $query->addSelect('text')->get();
//        $articles = DB::table('articles')
//                            ->select('text','id')
//                            ->where('id','>',5)
//                            ->where('text','like','text%', 'or')
//                            ->get();

//        $articles = DB::table('articles')
//            ->select('text','id')
//            ->where([
//                        ['id','>',5],
//                        ['text','like','text%', 'or']
//                    ])
//            ->get();


//        $articles = DB::table('articles')->whereBetween('id',[1,5])->get();
//        $articles = DB::table('articles')->whereIn('id',[1,2,3,5])->get();
//        $articles = DB::table('articles')->whereNotIn('id',[1,2,3,5])->get();
//        $articles = DB::table('articles')->groupBy('name')->get();
//        $articles = DB::table('articles')->take(4)->get();//"select * from `articles` limit 4"
//        $articles = DB::table('articles')->take(4)->skip(2)->get();//"select * from `articles` limit 4 offset 2"

//        $res = DB::table('articles')->insertGetId(['name' => 'Textetxt2', 'text' => '$oto text']);
//
//        $articles = DB::table('articles')->where('id',$res)->get();


//        DB::table('articles')->where('id',11)->update(['name' => '111111', 'text' => '2222222']);

        DB::table('articles')->where('id',11)->delete();

        $articles = DB::table('articles')->get();

        dump($articles);
    }
    //material
    public function getArticle($id)
    {
        echo $id;
    }

}
