@extends('default.layouts.layouts')

{{--  Переопределение шаблона  --}}
@section('navbar')
    @parent {{-- Отображение родительской секции --}}
@endsection


@section('header')
    @parent
@endsection

@section('sidebar')
    @parent

    <h2>sidebar test</h2>
    <p>Donec id elit non mi  </p>

@endsection

@section('content')

    @include('default.content')

@endsection