@extends('default.layouts.layouts')

@section('navbar')
    @parent
@endsection


@section('header')
    @parent
@endsection

@section('sidebar')


@endsection

@section('content')

    @if(count($errors) > 0)
        <ul>
            @foreach($errors->all() as $i)
                <li>{{ $i }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('contact' /* ['name' => 'hello'] */ )  }}" method="POST">

        <div class="form-group">
            <label>Name:</label>
            <input type="text" name="name" value="{{ old('name') }}" class="form-control">
        </div>

        <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" value="{{ old('email') }}" class="form-control">
        </div>

        <div class="form-group">
            <label>Site:</label>
            <input type="text" name="site" value="{{ old('site') }}" class="form-control">
        </div>

        <div class="form-group">
            <label>Text:</label>
            <textarea type="text" name="text" class="form-control">{{ old('text') }}</textarea>
        </div>

        {{ csrf_field() }}

        <button type="submit" class="btn btn-default">Submit</button>

    </form>

@endsection