
{{ $script  }}

{{-- Запред преобразования в html сущности --}}
{{--  {!! $script  !!} --}}

{{-- Строка будет отображена без изменений --}}
@{{ var  }}

{{-- {{ isset($bvar) ? $bvar : $title  }} == {{ $bvar or $title  }} --}}
@if(count($data) < 3)
    Меньше 3
@elseif(count($data) > 10)
    Больше 10
@else
    неизвестно
@endif

<ul>
    @for($i = 0; $i < count($dataI); $i++)
        <li>{{ $dataI[$i]  }}</li>
    @endfor
</ul>

<ul>
    @foreach($data as $k=>$v)
        <li>{{ $k."=>".$v  }}</li>
    @endforeach
</ul>

<ul>
    <?php $data_empty = []  ?>
    {{-- если массив пуст то работает оператор @empty --}}
    @forelse($data_empty as $k=>$v)
        <li>{{ $k."=>".$v  }}</li>
    @empty
        <p>No items</p>
    @endforelse
</ul>

@each('default.list', $dataI, 'value')

@myDir('Hello');



<div class="col-md-4">
    <h2>Heading</h2>
    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
    <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
</div>

<div class="col-md-4">
    <h2>Heading</h2>
    <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
    <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a></p>
</div>
