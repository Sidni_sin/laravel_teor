<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');//id INT AUTO_INCREMENT PRIMARY KEY
            $table->string('name', 100);// VARCHAR 100
            $table->text('text');// TEXT
            $table->string('img', 255)->nullable()->default(null);
            $table->string('alias', 100)->nullable()->default(null);
            $table->timestamps();//2 поля 1) -

            //https://laravel.ru/docs/v5/migrations

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
