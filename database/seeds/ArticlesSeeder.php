<?php

use App\Article;
use Illuminate\Database\Seeder;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          //1
//        DB::insert('INSERT INTO `articles` (`name`,`text`,`img`) VALUES (?,?,?)',
//                    [
//                        'Blog post',
//                        '<p>Text aticles</p>',
//                        'pic1.jpg',
//                    ]
//        );

        //2
//        DB::table('articles')->insert([
//            ['name'=>'Blog post 1', 'text'=>'Text articles 1', 'img'=>'poc1.jpg'],
//            ['name'=>'Blog post 2', 'text'=>'Text articles 2', 'img'=>'poc2.jpg'],
//            ['name'=>'Blog post 3', 'text'=>'Text articles 3', 'img'=>'poc3.jpg'],
//        ]);

        //3
        Article::create([
            'name' => 'Blog post1',
            'text' => 'Hello world',
            'img' => 'pic1.jpg',
        ]);

    }
}
